function [X] = CMTC (M, P0, T)
n=size(M,1);
for i=1:n
    Q(i,:)=-M(i,:)/M(i,i);
    Q(i,i)=0;
end
X(1) = loi_empirique(P0,(1:n));
i=1;
t(1)=loi_exp(-M(X(i),X(i)));
k=0;u=0;
while (t(i)<T)
    X(i+1)=loi_empirique(Q(X(i),:),(1:n));
    S=loi_exp(-M(X(i+1), X(i+1)));
    t(i+1)=t(i)+S;
    if X(i+1)>1
        k=k+S;
        u=u+1;
    end
    i=i+1;
end
k
toperte=(length(X)-u)/length(X)
tooccup=k/T
debi=u/T

