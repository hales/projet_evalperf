function [txPertes, txOccupation, debit, tMoySejour] = lecturemetafile (n)

matArr=load('TArrivee.mat', 'X');
lambda=mean(matArr.X);

Z=load('TTraitement.mat', 'X');
mu=mean(Z.X);
nbrEtats=n+2;
M=zeros(nbrEtats,nbrEtats);
M(1,1)=-1/lambda;
M(1,2)=1/lambda;
for i=2:(nbrEtats-1)
   M(i,i)=-1/lambda-1/mu;
   M(i,i-1)=1/mu;
   M(i,i+1)=1/lambda;
end
M(nbrEtats,nbrEtats)=-1/mu;
M(nbrEtats,nbrEtats-1)=1/mu;
matEtats=zeros(1,nbrEtats);
matEtats(1)=1;

Q=zeros(nbrEtats,nbrEtats);
for i=1:nbrEtats
    Q(i,:)=-M(i,:)/M(i,i);
    Q(i,i)=0;
end
A(1) = loi_empirique(matEtats,(1:nbrEtats));

i=1;
t(1)=loi_exp(-M(A(1), A(1)));
k=0;u=0;m=0;v=0;
while (t(i)<60)

    A(i+1)=loi_empirique(Q(A(i),:),(1:nbrEtats));
    S=loi_exp(-M(A(i+1), A(i+1)));
    t(i+1)=t(i)+S;
    
    if A(i+1)>1
        k=k+S;
        u=u+1;
    end
    if(A(i+1) == nbrEtats)
        v=v+S;
    end
    i=i+1;
end

tMoySejour = k/u;
txOccupation=k/60;
debit=u/60;
txPertes=v/t(i);

