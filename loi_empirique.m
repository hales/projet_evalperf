function [X] = loiEmpirique(P,Y)
[M,N]=size(P);
F = [];
F(1) = P(1);
for i=2:N
    F(i) = F(i-1) + P(i);
end
U = rand;
index =[];
index = find (F > U); %Trouve la premiere position supérieure à U
X = Y(index(1));
end
