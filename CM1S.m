function [txOcuup, debit, txPertes] = CM1S ()

%Arrivée
matArr=load('TArrivee.mat', 'X');
moyArrivee=mean(matArr.X);

%Traitement
%Matrice génératrice
M=[-1/moyArrivee 1/moyArrivee 
    4000 -4000];
p0=[1 0];%Etat initial
nbrEtats=size(M,1);
Q=zeros(2,2);
for i=1:nbrEtats
    Q(i,:)=-M(i,:)/M(i,i);
    Q(i,i)=0;
end
A=[];
A(1) = loi_empirique(p0,(1:nbrEtats));
nbrEtats=2;
i=1;
t(1)=loi_exp(-M(A(1),A(1)));
k=0;u=0;v=0;
while (t(i)<60)
    
    A(i+1)=loi_empirique(Q(A(i),:),(1:nbrEtats));
    S=loi_exp(-M(A(i+1), A(i+1)));
    t(i+1)=t(i)+S;
    if A(i+1)>1
        k=k+S;
        u=u+1;
    end
    if(A(i+1) == nbrEtats)
        v=v+S;
    end
    i=i+1;
end

txOcuup=k/60;
debit=u/60;
txPertes=v/t(i);